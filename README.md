# OpenML dataset: US-2020-Presidential-Election-Speeches

https://www.openml.org/d/43709

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The goal of this dataset is to provide a tidy way to access to the transcripts of speeches given by various US politicians in the context of the 2020 US Presidential Election. Transcripts have been scraped from rev.com. Some other information, such as location and type of speech, have been manually added to the dataset. 
Content
The dataset has the following columns:
speaker: Who gave the speech
title: a title or a description of speech
text: the transcript of the speech
location: the location or the platform where the speech was give
type: type of speech (e.g., campaign speech, interview or debate)
Acknowledgements
We wouldn't be here without the help of others. If you owe any attributions or thanks, include them here along with any citations of past research.
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43709) of an [OpenML dataset](https://www.openml.org/d/43709). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43709/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43709/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43709/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

